# %% Functions for processing data
import os
import json
import numpy as np
from numpy.fft import rfft, fft
from typing import Dict, Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    abs_value = np.array([])
    for i in range(0, len(x)):
        abs_value= np.append(abs_value, ((x[i]**2+y[i]**2+z[i]**2)**0.5))
    return(abs_value)

def interpolation(time: np.ndarray, data: np.ndarray, int_time: np.ndarray) -> np.ndarray:
    """Linearly interpolates values in data.

    Args:
        time (ndarray): Timestamp of the values in data
        data (ndarray): Values to interpolate
        int_time (ndarray): Points in time at which the data is to be interpolated.

    Returns:
        int_data (ndarray): Interpolation points based on 'time'.
    """
    
    vec_time1 = np.array([])
    for i in time:
        vec_time1 = np.append(vec_time1, i-time[0])
    time = vec_time1
    
    n = len(int_time)
    
    data_int = np.array([])
    
    
    data_int = np.append(data_int, data[0])
    
    for i in range(1,n-1):
        idx = np.abs(time - int_time[i]).argmin()
        if idx<len(time)-1:
            if int_time[i]>=time[idx]:
                a = idx
                b = idx+1
            else :
                a = idx-1
                b = idx
            
        
        
        m = (data[b] - data[a])/(time[b]-time[a])

        k = data[b] - m*time[b]
        
        acc_int = m*int_time[i] + k
        data_int = np.append(data_int, acc_int)
    
    data_int = np.append(data_int, data[n-1])
    
    return(data_int)

def my_fft_scaled(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x (with numpy fft() or rfft()) and scales the FFT amplitude.

    It is assumed that the time interval between the sampled sensor data
    is constant.

    Args:
        x (ndarray): Measurement data that are transformed into the
            frequency domain.
        time (ndarray): Timestamp of the measurement data

    Returns:
        (ndarray): Scaled Amplitude of the computed FFT spectrum
        (ndarray): Frequency of the computed FFT spectrum
    """
    
    
    
    X = fft(x)
    N = len(X)
    
    
    sr = len(time)/time[-1]
    
    fstep = sr/N
    freq = np.linspace(0, (N-1)*fstep, N)
    
    X[(freq<0.1)] = 0
    
    X= X/N
    
    X = abs(X)
    return(X, freq)


def evaluate_measurement_metadata(folder_path_metadata: str) -> Dict:
    """Finds the path to the JSON setup file and generates the setup_dict from it.

    Args:
        folder_path_metadata (str): Path to the folder with all relevant JSON metadata files, including JSON setup file.

    Returns:
        dict: Information in the setup_json_path file, enriched with the paths to the JSON files of the components.
    """
    for dir_path, _, file_names in os.walk(folder_path_metadata):
        for file_name in file_names:
            if file_name.endswith((".json")):
                file_path = os.path.join(dir_path, file_name)
                with open(file_path, "r") as json_file:
                    json_content = json.load(json_file)
                    if "setup" in json_content.keys():
                        setup_json_path = file_path
    return evaluate_setup(setup_json_path, folder_path_metadata)


def evaluate_setup(setup_json_path: str, folder_path_metadata: str) -> Dict:
    """Scans all metadata specified in the file with the path equal to setup_json_path.

    Args:
        setup_json_path (str): Path to setup JSON file.
        folder_path_metadata (str): Path where the JSON files of the components are located.

    Raises:
        RuntimeError: Is triggered if the information in the component JSON file does not match the information
            in the JSON-Setup file.

    Returns:
        dict: Information in the setup_json_path file, enriched with the paths to the JSON files of the components.
    """
    with open(setup_json_path, "r") as json_file:
        json_content = json.load(json_file)

    setup_dict = json_content["setup"]

    for dir_path, _, file_names in os.walk(folder_path_metadata):
        for file_name in file_names:
            if file_name.endswith((".json")):
                file_path = os.path.join(dir_path, file_name)
                with open(file_path, "r") as json_file:
                    json_content = json.load(json_file)
                    json_file_uuid = json_content["JSON"]["ID"]
                if json_file_uuid in setup_dict.keys():
                    json_file_type = list(json_content.keys())[1]
                    if setup_dict[json_file_uuid]["type"] == json_file_type:
                        setup_dict[json_file_uuid]["path"] = file_path
                    else:
                        print(
                            "Something is wrong with {} or the corresponding entry in the JSON setup file.".format(
                                file_path
                            )
                        )
                        raise RuntimeError(
                            "Metadata in JSON files is not specified correctly."
                        )

    return setup_dict


def extract_uuid(setup_dict: Dict) -> Tuple[str, str]:
    """Extratcts uuid of measuring devices from setup_dict.

    Args:
        setup_dict (dict): Information in the setup_json_path file.

    Returns:
        id_accelerometer (str): Uuid of 'accelerometer', required to read the acceleration from the h5 file
        id_hall (str):  Uuid of 'motor_controller', required to read the motor-rpm from the h5 file.
    """
    id_accelerometer = None
    id_hall = None
    for component_dict, component_uuid in zip(setup_dict.values(), setup_dict.keys()):
        if "sensor" in component_dict["type"]:
            if component_dict["name"] == "accelerometer":
                if id_accelerometer is None:
                    id_accelerometer = component_uuid
                else:
                    print("Several 'acccelerometers' defined in the JSON setup file")

        elif "instrument" in component_dict["type"]:
            if component_dict["name"] == "motor_controller":
                if id_hall is None:
                    id_hall = component_uuid
                else:
                    print("Several 'motor_controller' defined in the JSON setup file")

    if (id_accelerometer is None) or (id_hall is None):
        raise RuntimeError(
            "Relevant information is missing in the JSON setup file."
        )
    return id_accelerometer, id_hall
